var morseAlph = {
  0: "-----",
  1: ".----",
  2: "..---",
  3: "...--",
  4: "....-",
  5: ".....",
  6: "-....",
  7: "--...",
  8: "---..",
  9: "----.",
  a: ".-",
  b: "-...",
  c: "-.-.",
  d: "-..",
  e: ".",
  f: "..-.",
  g: "--.",
  h: "....",
  i: "..",
  j: ".---",
  k: "-.-",
  l: ".-..",
  m: "--",
  n: "-.",
  o: "---",
  p: ".--.",
  q: "--.-",
  r: ".-.",
  s: "...",
  t: "-",
  u: "..-",
  v: "...-",
  w: ".--",
  x: "-..-",
  y: "-.--",
  z: "--..",
  space: "&nbsp; &nbsp;"
};


var engAlph = {
  0: "0",
  1: "1",
  2: "2",
  3: "3",
  4: "4",
  5: "5",
  6: "6",
  7: "7",
  8: "8",
  9: "9",
  a: "a",
  b: "b",
  c: "c",
  d: "d",
  e: "e",
  f: "f",
  g: "g",
  h: "h",
  i: "i",
  j: "j",
  k: "k",
  l: "l",
  m: "m",
  n: "n",
  o: "o",
  p: "p",
  q: "q",
  r: "r",
  s: "s",
  t: "t",
  u: "u",
  v: "v",
  w: "w",
  x: "x",
  y: "y",
  z: "z",
  space: "&nbsp; &nbsp;"
};

var morseStr = " ";

//regular expression for english alphabet
var letters = /^[A-Za-z0-9]+$/;

//initialize variable to text-input object
var textInput = document.getElementById("text-input");

//Add onChange event handler to text-input object
textInput.addEventListener("input", onInput);

//translate morse code to english
function morseToEnglish(val){
  var arr = [];
  var index = Object.values(morseAlph).indexOf(val);
  //console.log(Object.values(engAlph)[index]);
  this.moreStr += Object.values(engAlph)[index];
  return this.moreStr;
}

//translate english to morse code
function englishToMorse(val){
  var arr = [];
  for(i=0; i <=val.length - 1; i++){
    if(val[i] == " ")
    {
      arr.push(morseAlph["space"]);
    }
    else{
      arr.push(morseAlph[val[i].toLowerCase()]);
    }
  }
  return arr.join('');
}


function onInput(e){
  var x = document.getElementById("text-input").value;
  if(e.target.value.match(letters) || e.target.value.includes(""))
  {
    if(textInput.value == ""){
      document.getElementById('msg').innerHTML = 'Nothing is being translated';
    }
    else{
      document.getElementById("msg").innerHTML = 'Translating English to Morse ...';
    }
    document.getElementById("text-output").innerHTML = englishToMorse(e.target.value.toLowerCase());

  }
  else{
    document.getElementById("msg").innerHTML = 'Translating Morse to English ...';
    //document.getElementById("text-output").innerHTML = morseToEnglish(e.target.value.toLowerCase());
    console.log(document.getElementById("text-input").value);
  }
}

function playAudio(){

}

function hightlightButton(){

}

//console.log(Object.values(morseAlph).indexOf('....-'));
//console.log(Object.values(morseAlph));
